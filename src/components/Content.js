import { Component } from "react";
import steps from "./Step";

class Content extends Component{
    render(){
        return (
            <><h3>Steps</h3>
                {steps.map((value,index) => {
                    return (
                        <ui key={index}>
                            <li>{value.id}. {value.title} : {value.content}</li>
                          
                        </ui>

                    
                    )
                })}
            </>
        )
    }
}

export default Content;